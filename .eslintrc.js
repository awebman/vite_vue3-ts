module.exports = {
  env: {
    browser: true,
    es2021: true,
    'vue/setup-compiler-macros': true
  },
  extends: [
    // 'plugin:vue/essential',
    'plugin:vue/vue3-strongly-recommended',
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  },
  plugins: [
    'vue',
    '@typescript-eslint'
  ],
  rules: {
    'vue/multi-word-component-names': 0 // 文件夹名字别要求多个组合字了
  },
  overrides: [{
    files: ['src/api/**/*.ts'],
    rules: {
      camelcase: 'off'
    }
  }],
  globals: { defineOptions: 'writable' }
}
